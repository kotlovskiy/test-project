export declare function resizeCanvas(canvas: HTMLCanvasElement, width: number, height: number, resizeCanvas?: boolean): void;
export declare function fitImageToAspectRatio(srcBase64: string, aspectRatio: number): Promise<string>;
