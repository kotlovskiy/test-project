import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Provider } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing-module';


import { MainLayoutComponent } from './shared/components/main-layout/main-layout.component';
import { HeaderComponent } from './shared/components/header/header.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { ProfileEditComponent } from './profile/profile-edit/profile-edit.component';
import { ProfileViewComponent } from './profile/profile-view/profile-view.component';
import { MapComponent } from './list-pages/map/map.component';
import { ListComponent } from './list-pages/list/list.component';
import { ImageCropperComponent } from './shared/components/image-cropper/image-cropper.component';



import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatInputModule } from '@angular/material/input';
import { MatDialogModule } from '@angular/material/dialog';
import { MatRadioModule } from '@angular/material/radio';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatPaginatorModule } from '@angular/material/paginator';


import { AuthInterceptor } from './auth.interceptor';
import { AuthGuard } from './shared/services/auth.guard';
import { ImageCropperModule } from '../assets/plugin/ngx-image-cropper';
import { ngfModule, ngf } from "angular-file";
import { LoadableImageComponent } from './shared/components/loadable-image/loadable-image.component'
import { AgmCoreModule } from '@agm/core';





const INTERCEPTOR_PROVIDER: Provider = {
  provide: HTTP_INTERCEPTORS,
  useClass: AuthInterceptor,
  multi: true
}


@NgModule({
  declarations: [
    AppComponent,
    MainLayoutComponent,
    HeaderComponent,
    LoginPageComponent,
    ProfileEditComponent,
    ProfileViewComponent,
    MapComponent,
    ListComponent,
    ImageCropperComponent,
    LoadableImageComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    AppRoutingModule,
    HttpClientModule,
    MatIconModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatInputModule,
    MatDialogModule,
    ImageCropperModule,
    ngfModule,
    MatRadioModule,
    AgmCoreModule.forRoot({
      apiKey: '',
      libraries: ['geometry']
    }),
    FormsModule,
    ReactiveFormsModule,
    MatPaginatorModule,
    MatTableModule,
    MatCardModule
  ],
  exports: [
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatInputModule,
    MatDialogModule,
    ImageCropperModule,
    ngfModule,
    LoadableImageComponent,
    MatRadioModule
  ],
  entryComponents: [
    ImageCropperComponent
  ],
  providers: [INTERCEPTOR_PROVIDER, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
