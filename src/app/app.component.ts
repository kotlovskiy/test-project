import { Component, OnInit } from '@angular/core';
import { LocationService } from './shared/services/location.servicse';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(private locationServices: LocationService) {}
  
  ngOnInit(): void {
      this.locationServices.getUserLocation()
      //to track user position
      setInterval(() => {this.locationServices.checkUserLocationOffset() }, 10000);
  }
}
