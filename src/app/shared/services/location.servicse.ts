import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap } from 'rxjs/operators'
import { User, Token } from '../interfaces';
import { Observable } from 'rxjs';

import { MapsAPILoader } from '@agm/core';


@Injectable({ providedIn: 'root' })

export class LocationService {

  currentUserNavigate: marker;
  previousUserNavigate: marker;

  constructor(private http: HttpClient, private mapsAPILoader: MapsAPILoader) { }

  getUserLocation() {
    if (navigator.geolocation) {
      // Геолокация доступна
      //alert("ok")
      navigator.geolocation.getCurrentPosition(function (position) {

        this.currentUserNavigate = {
          lat: position.coords.latitude,
          lon: position.coords.longitude
        }

        this.setCurrentUserLocation(this.currentUserNavigate)

        this.previousUserNavigate = {
          lat: 49.542760,
          lon: 28.3990203
        }

        //alert(`lat:${this.currentUserNavigate.lat} lng:${this.currentUserNavigate.lng}`)
      }.bind(this));
    }
    else {
      // Геолокация не доступна
      //alert("noo")
    }
    return this.currentUserNavigate
  }

  getDistanceInKm(point1, point2) {
    // this.mapsAPILoader.load().then(() => {
      const center = new google.maps.LatLng(point1.lat, point1.lon);
      const markerLoc = new google.maps.LatLng(point2.lat, point2.lon);
      const distanceInKm = google.maps.geometry.spherical.computeDistanceBetween(markerLoc, center) / 1000;
      console.log(`distance: ${distanceInKm}`)
      return +distanceInKm;
    // });
  }



  checkUserLocationOffset() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function (position) {
        this.currentUserNavigate = {
          lat: position.coords.latitude,
          lon: position.coords.longitude
        }
        if( (this.getDistanceInKm(this.currentUserNavigate, this.previousUserNavigate)) < 1) {
            this.currentUserNavigate = {
              lat: position.coords.latitude,
              lon: position.coords.longitude
            }
            this.previousUserNavigate = {
              lat: position.coords.latitude,
              lon: position.coords.longitude
            }
        } else {
          console.log("no")
        }
      }.bind(this));
    } 
  }



  setCurrentUserLocation(LatLng):Observable<Location> {
    return this.http.put<Location>('https://test-api.live.gbksoft.net/rest/v1/user/location', LatLng)
  }


}

interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}


