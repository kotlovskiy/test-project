import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap } from 'rxjs/operators'
import { User, Token } from '../interfaces';
import { Observable, Subscription, Subject } from 'rxjs';


@Injectable({providedIn:'root'})

export class UserService{

  currentUserList:[User];
  pageSize:number = 5;
  pageIndex:number = 1;

  changeUserList$: Subject<any> = new Subject<any>()

  constructor(private http: HttpClient) {}

  setImg(formData):Observable<any>{
    console.log("setImg")
    return this.http.post('https://test-api.live.gbksoft.net/rest/v1/user/profile/image', formData)
  }

  setData(data):Observable<any>{
    return this.http.put('https://test-api.live.gbksoft.net/rest/v1/user/profile', data)
  }

  getUserList(perPage, page):Observable<any>{
    return this.http.get(`https://test-api.live.gbksoft.net/rest/v1/user?perPage=${perPage}&page=${page}`)
    //this.changeUserList$.next();
  }

  userSearch(searchString){
    this.http.get(`https://test-api.live.gbksoft.net/rest/v1/user/search?perPage=${this.pageSize}&page=${this.pageIndex}&searchString=${searchString}`).subscribe(res=>{
      console.log(res);
      this.changeUserList$.next(res);
    })
  }

  getUserById(id):Observable<any>{
    return this.http.get(`https://test-api.live.gbksoft.net/rest/v1/user/${id}`)
  }
}