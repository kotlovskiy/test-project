import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap } from 'rxjs/operators'
import { User, Token } from '../interfaces';
import { Observable } from 'rxjs';

@Injectable({providedIn:'root'})

export class AuthService{

  user: User;
  userToken: Token;

  constructor(private http: HttpClient) {}

  get token(): string {
    const expDate = new Date(localStorage.getItem('user-token-expiredAt'))
    if(new Date() > expDate) {
      this.logout()
      return null
    }
    return localStorage.getItem('user-token')
  }

  login():Observable<any> {
    return this.http.post('https://test-api.live.gbksoft.net/rest/v1/user/login/facebook', { token:'EAAXQ9ea27L8BAFkO5RCibm9lgqruESUD1T0Niv7vc7vUanOguvY4UNcdnfXRAhVNToc8czKXai7M7jzNjfLu5lMicowme5ZBTkwD0R7GZCZBfW36Du5aFtdqm0quH91yPyWo2esBVi242R7N24i1ZBiRp5YXDnyiTO6MDTNsSsJh2xHhNwkImQi6tBgHOI5DaZAYK6RvVBwZDZD'})
    .pipe(
      tap(this.setToken)
    )
  }

  //для настоящей регистрации через фб

  // login(accessToken):Observable<any> {
  //   return this.http.post('https://test-api.live.gbksoft.net/rest/v1/user/login/facebook', { token:accessToken})
  //   .pipe(
  //     tap(this.setToken)
  //   )
  // }

  getCurrentUser():Observable<any>{
    return this.http.get('https://test-api.live.gbksoft.net/rest/v1/user/current').pipe(tap(res=>{
      localStorage.setItem('user-id', res.result.id)
    }))
  }

  logout() {
    this.setToken(null)
    // this.http.post('https://test-api.live.gbksoft.net/rest/v1/user/logout', null).subscribe(res=>{
    //   console.log("ok");
    // })
  }

  isAuthenticated(): boolean {
    return !!this.token
  }

  private setToken(response) {
    console.log(response)
    if(response) {
      const expDate = new Date(new Date().getTime() + +response.result.expiredAt * 1000)
      localStorage.setItem('user-token', response.result.token)
      localStorage.setItem('user-token-expiredAt', response.result.expiredAt)
    } else {
      localStorage.clear()
    }

  }


}