export interface User{
  id: string
  country: string
  city:string
  firstName:string
  lastName:string
  email: string
  image:string
  lat:number
  lon:number
  gender:string
  createdAt:string
  updatedAt:string
}

export interface Token{
    token: string
    expiredAt: string
}
export interface Location{
  lat: number
  lon: number
}