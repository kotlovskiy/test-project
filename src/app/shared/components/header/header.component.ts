import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { UserService } from '../../services/user.services';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  matButtonToggleGroup = "List"
  searchSubject: Subject<string> = new Subject<string>();
  
  constructor(private userService: UserService) { }

  ngOnInit() {

    this.searchSubject
    .pipe(
      debounceTime(800),
    )
    .subscribe((res) => {
      console.log(res);
      if(res === ''){
        this.userService.changeUserList$.next(false);
      } else {
        this.userService.userSearch(res);
      }
    });

  }

}
