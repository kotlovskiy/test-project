import { Component, OnInit, SimpleChanges, EventEmitter, HostBinding, Output, Input } from '@angular/core';
import { Observer, Observable } from 'rxjs';
import { ImageCropperComponent } from 'src/app/shared/components/image-cropper/image-cropper.component';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-loadable-image',
  templateUrl: './loadable-image.component.html',
  styleUrls: ['./loadable-image.component.scss']
})
export class LoadableImageComponent implements OnInit {


  @Input() placeHolder: string;
  @Input() initialImage: string;
  // @Input() width: number;
  // @Input() height: number;
  @Input() aspectRatio: number = 1;
  @Input() editingText: string;
  @Input() editing: boolean = false;

  @Output() onChange: EventEmitter<File> = new EventEmitter();

  @HostBinding('style.padding-bottom') paddingBottom = '100%';

  _imageFile: File;

  croppedImage: ImageCroppedEvent;

  // croppedImage: ImageCroppedEvent = {
  //   base64: null,
  //   file: null,
  //   width: null,
  //   height: null,
  //   cropperPosition: null,
  //   imagePosition: null,
  // };

  set imageFile(file: File) {
    console.log("imageFile")
    if (file instanceof File) {
      console.log("instanceof")
      this.reset();
      this._imageFile = file;
      const reader = new FileReader();
      reader.readAsDataURL(this._imageFile);
      reader.onload = () => {
        this.openCropper(reader.result as String);
      }
      reader.onerror = () => {
      }
    }
  }

  constructor(private dialog: MatDialog) { }

  ngOnInit() {
    this.reset();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes.aspectRatio && changes.aspectRatio.currentValue) {
      this.paddingBottom = (1 / this.aspectRatio) * 100 + '%';
    }
  }

  openCropper(bs64: String) {

    let croppedImage: ImageCroppedEvent;
    const that = this;

    let data = {
      maintainAspectRatio: true,
      imageBase64: bs64,
      aspectRatio: this.aspectRatio,
      // resizeToWidth: 50,
      // cropper: this.croppedAvatar.cropperPosition ? this.croppedAvatar.cropperPosition : null,
      format: "png",
      imageCropped(event) { croppedImage = event; that.onChange.emit(event.file) },
      imageLoaded(event) { },
      cropperReady(event) { },
      loadImageFailed(event) { },
    }
    const croperRef = this.dialog.open(ImageCropperComponent, {
      width: '1100px',
      panelClass: 'CropDialog',
      data,
    });

    croperRef.afterClosed().subscribe(result => {
      if (result) {
        this.croppedImage = croppedImage;
      }
    });
  }

  reset() {
    this.croppedImage = {
      base64: null,
      file: null,
      width: null,
      height: null,
      cropperPosition: null,
      imagePosition: null,
    };
  }

  getBase64ImageFromURL(url: string) {
    return Observable.create((observer: Observer<string>) => {
      // create an image object
      let img = new Image();
      img.crossOrigin = 'anonymous';
      img.src = url;
      if (!img.complete) {
        // This will call another method that will create image from url
        img.onload = () => {
          observer.next(this.getBase64Image(img));
          observer.complete();
        };
        img.onerror = (err) => {
          observer.error(err);
        };
      } else {
        observer.next(this.getBase64Image(img));
        observer.complete();
      }
    });
  }

  getBase64Image(img: HTMLImageElement) {
    // We create a HTML canvas object that will create a 2d image
    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;
    var ctx = canvas.getContext("2d");
    // This will draw image    
    ctx.drawImage(img, 0, 0);
    // Convert the drawn image to Data URL
    var dataURL = canvas.toDataURL("image/png");
    return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
  }

  editImage() {
    if (this._imageFile) {
      const reader = new FileReader();
      reader.readAsDataURL(this._imageFile);
      reader.onload = () => {
        this.openCropper(reader.result as String);
      }
      reader.onerror = () => {

      }
    } else {
      this.getBase64ImageFromURL(this.initialImage).subscribe(base64data => {
        this.openCropper('data:image/png;base64,' + base64data);
      })
    }
  }

  deleteImage() {
    this.croppedImage = {
      base64: null,
      file: null,
      width: null,
      height: null,
      cropperPosition: null,
      imagePosition: null,
    };
    this.onChange.emit(null);
    this._imageFile = null;
  }
}
