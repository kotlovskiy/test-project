import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { UserService } from 'src/app/shared/services/user.services';
import { User } from 'src/app/shared/interfaces';

@Component({
  selector: 'app-profile-view',
  templateUrl: './profile-view.component.html',
  styleUrls: ['./profile-view.component.scss']
})
export class ProfileViewComponent implements OnInit {

  user:User;
  authorizationUserId:string;

  constructor(private rout: ActivatedRoute, private userService: UserService) { }

  ngOnInit() {
    this.authorizationUserId = localStorage.getItem('user-id')
    this.rout.params.subscribe( (param: Params) => {
      this.userService.getUserById(param.id).subscribe(res=>{
        this.user = res.result
      })
    })
    
  }

}
