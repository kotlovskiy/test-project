import { Component, OnInit, OnDestroy} from '@angular/core';
import { UserService } from 'src/app/shared/services/user.services';
import { AuthService } from 'src/app/shared/services/auth.services';
import { FormGroup, FormControl, Validators, FormGroupDirective, NgForm } from '@angular/forms';
import { User } from 'src/app/shared/interfaces';
import { ErrorStateMatcher } from '@angular/material/core';
import { LocationService } from 'src/app/shared/services/location.servicse';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.component.html',
  styleUrls: ['./profile-edit.component.scss']
})
export class ProfileEditComponent implements OnInit, OnDestroy {
 
  userAvatar: File;
  userAvatarUrl: string;
  form:FormGroup;
  changeAvatar:boolean = false;
  formSubscriber: Subscription;
  matcher = new MyErrorStateMatcher();
  changeDate:boolean = false;
  constructor(private userService: UserService, private authServices: AuthService, private locationServices: LocationService){}

  asss(){
    console.log(this.form)
  }

  ngOnInit(): void {
    this.authServices.getCurrentUser().subscribe( res => {
      console.log(res)
      this.fillUserData(res.result as User)
    })
  }

  ngOnDestroy(): void {
    this.formSubscriber.unsubscribe();
  }

  fillUserData(data) {
    if(data.image) {
      this.userAvatarUrl = data.image;
    }
    this.form = new FormGroup({
      email: new FormControl(data.email,[Validators.required,Validators.email]),
      country: new FormControl(data.country),
      city: new FormControl(data.city),
      firstName: new FormControl(data.firstName,Validators.required),
      lastName: new FormControl(data.lastName,Validators.required),
      gender: new FormControl(data.gender)
    })
     this.formSubscriber = this.form.valueChanges.subscribe(val => {
      this.changeDate = true;
    });
  }

  saveUserData() {
    console.log(this.form)
    if(this.form.invalid) {
      return
    } else {
      let user: User = {...this.form.value};
      this.userService.setData(user).subscribe( res => {
        this.fillUserData(res.result)
        this.locationServices.setCurrentUserLocation(this.locationServices.currentUserNavigate).subscribe(res=>{
          this.changeDate = false;
          if(this.changeAvatar) {
            const coverFormData: FormData = new FormData();
            coverFormData.append('image', this.userAvatar, "avatar.png");
            this.userService.setImg(coverFormData).subscribe(res=>{
              this.changeDate = false;
            })
          } else {
            this.changeDate = false;
          }
        });
      })
    }
  }


  avatarChanged(img) {
    this.changeAvatar = true;
     this.changeDate = true;
    this.userAvatar = img;
    if (!img) {
      this.userAvatarUrl = null;
    }
  }

  // setUserImg(){
  //   const coverFormData: FormData = new FormData();
  //   coverFormData.append('image', this.userAvatar, "avatar.png");
  //   this.userService.setImg(coverFormData).subscribe(res=>{
  //     console.log("img update")
  //   })
  // }

  

}

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}