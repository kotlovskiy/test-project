import { Component, OnInit } from '@angular/core';
import { AuthService } from '../shared/services/auth.services';
import { Router } from '@angular/router';
declare var FB: any;

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    //для настоящей регистрации через фб
    // (window as any).fbAsyncInit = function () {
    //   FB.init({
    //     appId: '613738309359388',
    //     cookie: true,
    //     xfbml: true,
    //     version: 'v3.1'
    //   });
    //   FB.AppEvents.logPageView();
    // };

    // (function (d, s, id) {
    //   var js, fjs = d.getElementsByTagName(s)[0];
    //   if (d.getElementById(id)) { return; }
    //   js = d.createElement(s); js.id = id;
    //   js.src = "https://connect.facebook.net/en_US/sdk.js";
    //   fjs.parentNode.insertBefore(js, fjs);
    // }(document, 'script', 'facebook-jssdk'));
  }
  //для настоящей регистрации через фб
  // submitLogin() {
  //   FB.login();
  //   FB.login((response) => {
  //     console.log('submitLogin', response);
  //     if (response.authResponse.accessToken) {

  //     }
  //   });
  // }

  signIn() {
    this.authService.login().subscribe(res => {
      if (res) {
        this.router.navigate(['/profile-edit'])
      }
    })
  }


  logout() {
    this.authService.logout()
  }

}
