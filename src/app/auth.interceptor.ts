import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AuthService } from './shared/services/auth.services';

@Injectable()

export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log('interceptor' , req)

    let token = 'Bearer ' + this.authService.token;
    
    const cloned = req.clone({
      headers: req.headers.append('Authorization', token)
    })

    return next.handle(cloned)
  }

}