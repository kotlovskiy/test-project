import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { UserService } from 'src/app/shared/services/user.services';
import { User } from 'src/app/shared/interfaces';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  length = 100;
  pageSize = 5;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  displayedColumns: string[] = ['id', 'firstName', 'lastName', 'Email', 'btn'];
  usersList: User[];
  dataSource = new MatTableDataSource<any>(this.usersList);

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private userService: UserService) { }


  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.getUsers(this.pageSize, 1);

    this.userService.changeUserList$.subscribe(data=>{
      console.log(data)
      if(data) {
        this.dataSource = data.result;
        this.pageSize = data._meta.pagination.currentPage;
        this.length = data._meta.pagination.pageCount;
      } else {
        this.getUsers(5, 1);
      }
    })
  }
  getServerData(event) {
    this.pageSize = event.pageSize;
    this.getUsers(event.pageSize, ++event.pageIndex);
  }

  getUsers(perPage, page) {
    this.userService.getUserList(perPage, page).subscribe(res => {
      console.log(res)
      this.dataSource = res.result;
      this.pageSize = res._meta.pagination.currentPage;
      this.length = res._meta.pagination.pageCount;
    })
  }

}
