import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { UserService } from 'src/app/shared/services/user.services';
import { User } from 'src/app/shared/interfaces';
import { Router } from '@angular/router';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  length = 100;
  pageSize = 5;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  usersList: User[];
  pageEvent: PageEvent;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.getUsers(this.pageSize, 1);

    this.userService.changeUserList$.subscribe(data => {
      console.log(data)
      if (data) {
        this.usersList = data.result;
        this.pageSize = data._meta.pagination.currentPage;
        this.length = data._meta.pagination.pageCount;
      } else {
        this.getUsers(5, 1);
      }
    })

  }
  navigateProvile(id) {
    this.router.navigate(['/profile', id])
  }

  getUsers(perPage, page) {
    this.userService.getUserList(perPage, page).subscribe(res => {
      console.log(res)
      this.usersList = res.result;
      this.pageSize = res._meta.pagination.currentPage;
      this.length = res._meta.pagination.pageCount;
    })
  }

  getServerData(event) {
    this.pageSize = event.pageSize;
    this.getUsers(event.pageSize, ++event.pageIndex);
  }

  // google maps zoom level
  zoom: number = 13;

  // initial center position for the map
  lat: number = 49.232295;
  lng: number = 28.452289;

}






